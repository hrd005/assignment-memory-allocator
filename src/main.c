#include "test.h"

int main() {
    struct test_result res = normal_allocation_test();
    test_result_print(res);

    res = one_block_free_test();
    test_result_print(res);

    res = two_blocks_free_test();
    test_result_print(res);

    res = new_region_extends_test();
    test_result_print(res);

    res = new_region_not_extends_test();
    test_result_print(res);

    return 0;
}

