#ifndef ALLOC_TEST_H
#define ALLOC_TEST_H

#define TEST_BUFFER_SIZE 2000
#define TEST_EXTRA_BIG_BUFFER_SIZE 10000    // Heap size will be 5*4096 after allocating

enum test_status {
    TEST_PASSED = 0,
    TEST_FAILED = 1
};

struct test_result {
    char * name;
    enum test_status stat;
    char * msg;
};

struct test_result normal_allocation_test();
struct test_result one_block_free_test();
struct test_result two_blocks_free_test();
struct test_result new_region_extends_test();
struct test_result new_region_not_extends_test();

void test_result_print(struct test_result status);

#endif //ALLOC_TEST_H
